﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SQLiteWrapper;
using System.IO;

namespace Synchronizator
{
    public partial class Zadania : Form
    {
        public string obecnyKatalog;
        public string zadanieEdycja = "";
        public int idZadania=0;
        public bool pierwszeOdpalenie = true;
        
        
        public Zadania(string zadanie="")
        {
            InitializeComponent();
            obecnyKatalog = Directory.GetCurrentDirectory();

            if (zadanie != "")
            // nastepuje edycja danych
            {
                textBox1.Text = zadanie;
                zadanieEdycja = zadanie;
                AkutalizujKatalogi(zadanie);


                // pobranie id zadania

                SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

                DataTable table = dbSQLite.ExecuteQuery("SELECT `id`,`ftp` FROM `zadania` WHERE `nazwa` = '"+zadanie+"';");

                DataTable table2 = dbSQLite.ExecuteQuery("SELECT count(*) FROM `zadania` WHERE `nazwa` = '" + zadanie + "' AND `id` = '" + (int)table.Rows[0][0] + "';");

                if ((int)table2.Rows[0][0] == 1)
                {
                    idZadania = (int)table.Rows[0][0];
                }
                else
                    // wystapil bardzo dziwny i nie znany blad
                {
                    MessageBox.Show("Dziwne... cos nie fika - wystapil dziwny i nie zdiagnozowalny blad (: 'Don't worry be happy' ");
                    Close();
                }

                // sprawdzanie czy jest przypisane jakies polaczenie

                //MessageBox.Show(Convert.ToString((int)table.Rows[0][1]));

                if ((int)table.Rows[0][1] != 0)
                // jest przypisane
                {
                    panel2.Enabled = true;
                    checkBox1.Checked = true;
                }
                else
                {
                    panel2.Enabled = false;
                    checkBox1.Checked = false;
                }

                AktualizacjaFTP();

                dbSQLite.CloseDatabase();

                pierwszeOdpalenie = false;

            }
            else
                // dodawanie nowego zadania
            {
                // wylaczenie pol niedostepnych przed wprowadzeniem nazwy!
                button2.Enabled = false;
                button1.Enabled = false;
                comboKatalogi.Enabled = false;
                checkBox1.Enabled = false;

                MessageBox.Show("Wprowadz najpierw nazwę zadania i zapisz je!");


            }

        }

        
        public void AkutalizujKatalogi(string zadanie)
        {
            //aktualizuje comboboxa z katalogami

            comboKatalogi.Items.Clear();

            SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

            DataTable table = dbSQLite.ExecuteQuery("SELECT `id` FROM `zadania` WHERE `nazwa` = '"+zadanie+"';");

            foreach (DataRow row in table.Rows)
            {

                idZadania = (int) row[0];

                DataTable table2 = dbSQLite.ExecuteQuery("SELECT `dir` FROM `zadania_dir` WHERE `id_zadanie` = '" + row[0] + "';");

                foreach (DataRow row2 in table2.Rows)
                {
                    comboKatalogi.Items.Add(row2[0]);
                    comboKatalogi.SelectedIndex = 0;
                }
                
                
            }

            table.Clear();

            if (idZadania != 0)
            {
                //ustalanie ile jest zadan i edycja label
                table = dbSQLite.ExecuteQuery("SELECT count(*) FROM `zadania_dir` WHERE `id_zadanie` = '" + idZadania + "';");

                foreach (DataRow row in table.Rows)
                {
                    label2.Text = "Katalogi: ("+row[0]+" wszystkich)";
                }

            }
            
            


            dbSQLite.CloseDatabase();


        }
        
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            // udostepnianie dla FTP

            if (panel2.Enabled == true)
            {
                if (pierwszeOdpalenie != true)
                {
                    panel2.Enabled = false;
                }
                
                
                AktualizacjaFTP();
            }
            else
            {
                if (pierwszeOdpalenie != true)
                {
                    panel2.Enabled = true;
                }
                
                AktualizacjaFTP();
            }
             
            
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // usuowanie katalogu

            if (idZadania != 0)
            {
                SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

                //"DELETE FROM `zadania_dir` WHERE `id_zadanie` = '" + idZadania + "' AND `dir` = '" + comboKatalogi.GetItemText(comboKatalogi.SelectedItem) + "';";

                dbSQLite.ExecuteNonQuery("DELETE FROM `zadania_dir` WHERE `id_zadanie` = '" + idZadania + "' AND `dir` = '" + comboKatalogi.GetItemText(comboKatalogi.SelectedItem) + "';");

                dbSQLite.CloseDatabase();
                //comboKatalogi.GetItemText(comboKatalogi.SelectedItem);
            }

            AkutalizujKatalogi(zadanieEdycja);
            






        }

        private void button1_Click(object sender, EventArgs e)
        {

            // dodawanie nowego katalogu do zadania

            if (idZadania != 0)
            {
                FolderBrowserDialog wyborKatalogu = new FolderBrowserDialog();

                if (wyborKatalogu.ShowDialog() == DialogResult.OK)
                {
                    SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

                    // sprawdzanie, czy juz ten sam katalog nie jest dodawany do tego samego zadania

                    DataTable table = dbSQLite.ExecuteQuery("SELECT count(*) FROM `zadania_dir` WHERE `id_zadanie` = '" + idZadania + "' AND `dir` = '" + wyborKatalogu.SelectedPath + "';");

                    foreach (DataRow row in table.Rows)
                    {
                        if ((int)row[0] == 0)
                        {
                            //dodawanie nowego zadania
                            dbSQLite.ExecuteNonQuery("INSERT INTO `zadania_dir` (`id_zadanie`,`dir`) VALUES('" + idZadania + "','" + wyborKatalogu.SelectedPath + "');");
                            //aktualizacja comboboxa z katalogami
                            AkutalizujKatalogi(zadanieEdycja);
                        }
                        else
                        //nie wolno 2x tego samego folderu dodawac do jednego zadania!
                        {
                            MessageBox.Show("Nie możesz tego samego po raz kolejny dodać do zadania!");
                        }
                    }

                    

                    dbSQLite.CloseDatabase();
                }

                

            }
            else
            {
                MessageBox.Show("Musisz najpierw zapisać zadanie!");
            }
           
            



        }

        private void button4_Click(object sender, EventArgs e)
        {

            //zamykanie aplikacji

            Close();


        }

        private void button3_Click(object sender, EventArgs e)
        {

            // zapisywanie

            SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

            if ((zadanieEdycja!="") || (idZadania!=0))
                //edycja zadania - zapis
            {

                if (idZadania != 0 && zadanieEdycja != "" && textBox1.Text=="")
                // nazwa zadania nie moze pozostac pusta
                {
                    MessageBox.Show("Pole nazwa (zadania) nie moze pozostać puste!");
                }
                else
                {
                    // zapisywanie nowej nazwy oraz ewentualnej zmiany FTP

                    // sprawdzenie, czy nazwa jest unikalna

                    DataTable table = dbSQLite.ExecuteQuery("SELECT count(*) FROM `zadania` WHERE `nazwa` = '" + textBox1.Text + "';");

                    DataTable tablen = dbSQLite.ExecuteQuery("SELECT `nazwa` FROM `zadania` WHERE `id` = '" + idZadania + "';");


                    if ((int)table.Rows[0][0] == 0 || Convert.ToString(tablen.Rows[0][0]) == textBox1.Text)
                    //nazwa jest unikalna lub nie nastapila zmiana nazwy
                    {


                        dbSQLite.ExecuteNonQuery("UPDATE `zadania` SET `nazwa` = '" + textBox1.Text + "' WHERE `id` = '" + idZadania + "';");

                        // FTP

                        if (checkBox1.Checked == true)
                        {
                            //DataTable table = dbSQLite.ExecuteQuery("SELECT `ftp` FROM `zadania` WHERE `nazwa` = '" + textBox1.Text + "' AND `id` = '"+idZadania+"';");

                            // sprawdzenie jakie id ma wybrane poleczenie ftp


                            if (comboFTP.Items.Count != 0)
                            {
                                DataTable table2 = dbSQLite.ExecuteQuery("SELECT `id_ftp` FROM `ftp` WHERE `nazwa` = '" + comboFTP.GetItemText(comboFTP.SelectedItem) + "';");

                                dbSQLite.ExecuteNonQuery("UPDATE `zadania` SET `ftp` = '" + table2.Rows[0][0] + "' WHERE `id` = '" + idZadania + "';");
                            }

                            


                            //comboKatalogi.GetItemText(comboKatalogi.SelectedItem);
                        }
                        else
                        {
                            // nadawanie null w kolumnie do FTP

                            dbSQLite.ExecuteNonQuery("UPDATE `zadania` SET `ftp` = '0' WHERE `nazwa` = '" + textBox1.Text + "' AND `id` = '" + idZadania + "';");

                        }

                        // box podsumuwujacy edycje
                        MessageBox.Show("Zmiany zapisano pomyślnie!");


                    }
                    else
                        // nazwa nie jest unikalna
                    {
                        MessageBox.Show("Niestety któreś zadanie posiada już taką nazwę - podaj inną!");
                    }

                }





            }
            else
                //zapisywanie nowego zadania
            {

                //sprawdzanie czy nazwa sie nie powtarza
                
                DataTable table = dbSQLite.ExecuteQuery("SELECT count(*) FROM `zadania` WHERE `nazwa` = '"+textBox1.Text+"';");

                if ((int)table.Rows[0][0] == 0)
                    //nazwa jest unikalna
                {
                    dbSQLite.ExecuteNonQuery("INSERT INTO `zadania` (`nazwa`,`ftp`) VALUES('"+textBox1.Text+"',0);");

                    // pobieranie id zadania

                    table.Clear();

                    table = dbSQLite.ExecuteQuery("SELECT `id` FROM `zadania` WHERE `nazwa` = '"+textBox1.Text+"';");

                    foreach (DataRow row2 in table.Rows)
                    {
                        idZadania = (int)row2[0];
                    }

                    zadanieEdycja = textBox1.Text;
                    //odblokowywanie pol 
                    
                    button2.Enabled = true;
                    button1.Enabled = true;
                    comboKatalogi.Enabled = true;
                    checkBox1.Enabled = true;
                    checkBox1.Checked = false;

                    pierwszeOdpalenie = false;

                    AkutalizujKatalogi(zadanieEdycja);

                }
                else
                    // taka nazwa jest juz zapisana
                {
                    MessageBox.Show("Niestety któreś zadanie posiada już taką nazwę - podaj inną!");
                }
                
                    
                

            }


            dbSQLite.CloseDatabase();


        }

        private void button5_Click(object sender, EventArgs e)
        {
            // dodaj nowy FTP

            Enabled = false;

            KonfiguracjaFTP FTPform = new KonfiguracjaFTP();
            FTPform.ShowDialog();

            Enabled = true;

            // aktualizacja pola z FTP 
            AktualizacjaFTP();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            // edycja danego FTP

            if (comboFTP.Items.Count != 0)
            {
                Enabled = false;

                KonfiguracjaFTP FTPform = new KonfiguracjaFTP(comboFTP.GetItemText(comboFTP.SelectedItem));
                FTPform.ShowDialog();

                Enabled = true;

                // aktualizacja pola z FTP 
                AktualizacjaFTP();

            }
            else
            {
                MessageBox.Show("Brak FTP do edycji!");
            }

            

        }

        public void AktualizacjaFTP()
        {

            //sprawdzenie czy do zadania jest przypisany jakis ftp

            comboFTP.Items.Clear();

            SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

            int idFTPZadanie = 0;
            int licznik = 0;

            if (idZadania != 0)
            {

                //DataTable table2 = dbSQLite.ExecuteQuery("SELECT count(*) FROM `zadania` WHERE `id` = '" + idZadania + "';");

                //if (

                
                
                DataTable table = dbSQLite.ExecuteQuery("SELECT `ftp` FROM `zadania` WHERE `id` = '" + idZadania + "';");

                idFTPZadanie = (int)table.Rows[0][0];

            }
            else
            {
                idFTPZadanie = 0;
            }
            

            // wciskanie wartosci do pola combo

            DataTable table2 = dbSQLite.ExecuteQuery("SELECT `id_ftp`,`nazwa` FROM `ftp`;");

            if (table2.Rows.Count == 1)
            {
                //MessageBox.Show("robi sie count 1");
                comboFTP.Items.Add(table2.Rows[0][1]);
                comboFTP.SelectedIndex = 0;
            }
            else if (table2.Rows.Count > 1)
            {
                foreach (DataRow row in table2.Rows)
                {

                    comboFTP.Items.Add(row[1]);

                    if (idFTPZadanie == (int)row[0])
                    {
                        comboFTP.SelectedIndex = licznik;
                    }

                    licznik++;
                }

                if (idFTPZadanie == 0)
                {
                    comboFTP.SelectedIndex = 0;
                }

            }
            else
            {
                // nic sie nie dzieje
            }

            

            



            dbSQLite.CloseDatabase();



        }

        private void button7_Click(object sender, EventArgs e)
        {
            // usuniecie FTP danego

            if (comboFTP.Items.Count == 0)
                // brak do usuniecia
            {
                MessageBox.Show("Brak FTP do usuniecia");
            }
            else
                // jest co usuwac ;)
            {
                SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

                // pobranie wartosci id ftp

                DataTable table = dbSQLite.ExecuteQuery("SELECT `id_ftp` FROM `ftp` WHERE `nazwa` = '" + comboFTP.GetItemText(comboFTP.SelectedItem) + "';");

                // id ftp - table.Rows[0][0]

                // usuniecie FTP

                dbSQLite.ExecuteNonQuery("DELETE FROM `ftp` WHERE `nazwa` = '"+comboFTP.GetItemText(comboFTP.SelectedItem)+"';");

                // ustawienie id_ftp w zadaniach na wartosc domyslna

                dbSQLite.ExecuteNonQuery("UPDATE `zadania` SET `ftp` = '0' WHERE `ftp` = '" + table.Rows[0][0] + "';");

                dbSQLite.CloseDatabase();

                AktualizacjaFTP();
            }



        }

    }
}
