﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SQLiteWrapper;
using Ionic.Zip;
using System.Security.AccessControl;
using System.IO;

namespace Synchronizator
{
    public partial class Form2 : Form
    {

        public string obecnyKatalog;
        public object dbSQLite;
        
        public Form2()
        {
            InitializeComponent();
            obecnyKatalog = Directory.GetCurrentDirectory();
            AktualizacjaZadan();

            //File.Delete(string.Format(@"{0}\config.zip", obecnyKatalog));

        }

        

        public void AktualizacjaZadan()
        // uzupelnianie zadan do wyboru
        {

            comboBox1.Items.Clear();

            SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

            DataTable table = dbSQLite.ExecuteQuery("SELECT `nazwa` FROM `zadania`;");

            int licznik=0;

            foreach (DataRow row in table.Rows)
            {
                comboBox1.Items.Add(row[0]);
                licznik++;
            }

            if (licznik != 0)
            {
                comboBox1.SelectedIndex = 0; 
            }
            

            dbSQLite.CloseDatabase();


        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            // usunecie zadanie wraz ze wszystkimi przypisanymi katalogami



            if (comboBox1.Items.Count != 0)
            {
                SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

                // pobranie wartosci id zadania
                DataTable table = dbSQLite.ExecuteQuery("SELECT `id` FROM `zadania` WHERE `nazwa` = '" + comboBox1.GetItemText(comboBox1.SelectedItem) + "';");

                //id - table.Rows[0][0]

                // del all przypisanych katalogow + zadania

                dbSQLite.ExecuteNonQuery("DELETE FROM `zadania_dir` WHERE `id_zadanie` = '" + (int)table.Rows[0][0] + "';");
                dbSQLite.ExecuteNonQuery("DELETE FROM `zadania` WHERE `id` = '" + (int)table.Rows[0][0] + "';");

                dbSQLite.CloseDatabase();

                // refresh aktualnych zadan
                AktualizacjaZadan();
            }
            else
            {
                MessageBox.Show("Brak zadań do usunięcia...");
            }


            
                
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Nowe zadanie
            Enabled = false;
            
            Zadania zadaniaForm = new Zadania();
            zadaniaForm.ShowDialog();

            Enabled = true;

            AktualizacjaZadan();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox1.Items.Count != 0)
            {
                // Edycja istniejacego zadania
                Enabled = false;

                Zadania zadaniaForm = new Zadania(comboBox1.GetItemText(comboBox1.SelectedItem));
                zadaniaForm.ShowDialog();

                Enabled = true;

                AktualizacjaZadan();
            }
            else
            {
                MessageBox.Show("Brak zadań do edycji...");
            }
            
            
        }

        public static void RemoveDirectorySecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
        {
            // Create a new DirectoryInfo object.
            DirectoryInfo dInfo = new DirectoryInfo(FileName);

            // Get a DirectorySecurity object that represents the 
            // current security settings.
            DirectorySecurity dSecurity = dInfo.GetAccessControl();

            // Add the FileSystemAccessRule to the security settings. 
            dSecurity.RemoveAccessRule(new FileSystemAccessRule(Account, Rights, ControlType));

            // Set the new access settings.
            dInfo.SetAccessControl(dSecurity);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            // TUTAJ DOROBIC JESZCZE COS SIE BEDZIE DZIAC JAK BEDZIEMY ZAMYKAC!


            // tworzenie zipa z config

            SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

            DataTable table4 = dbSQLite.ExecuteQuery("SELECT `ConfigValue` FROM `config` WHERE `ConfigName` = 'haslo';");

            var zip = new ZipFile();

            zip.Password = Convert.ToString(table4.Rows[0][0]);
            //MessageBox.Show(string.Format("{0}{1}", obecnyKatalog, @"\temp\config"));

            zip.AddFile(string.Format("{0}{1}", obecnyKatalog, @"\temp\config"), @".\");
            //zip.AddFile(@".\temp\config",@".\");

            //RemoveDirectorySecurity(obecnyKatalog, @"", FileSystemRights.ReadData, AccessControlType.Allow);

            //Directory.SetAccessControl("obecnyKatalog", System.IO.FileAttributes.Normal);

            //MessageBox.Show(string.Format(@"{0}\{1}\config_soft\config.zip", obecnyKatalog, nazwaDirBackup));
            //zip.AddDirectory(string.Format("{0}{1}", obecnyKatalog, @"\temp\config\config");

            if(File.Exists(string.Format(@"{0}\config.zip", obecnyKatalog)));
            {
                //File.SetAttributes(string.Format(@"{0}\config.zip", obecnyKatalog), System.IO.FileAttributes.NotContentIndexed);
                
                
                //File.Delete(string.Format(@"{0}\config.zip", obecnyKatalog));
                
            }

            zip.Save(string.Format(@"{0}\config.zip", obecnyKatalog));
            
            dbSQLite.CloseDatabase();
            //~~~~ ODBLOKUJ!
            Directory.Delete(obecnyKatalog+@"\temp", true);

            
            Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            // przycisk synchronizacji!

            if (comboBox1.Items.Count != 0)
            {
                // Edycja istniejacego zadania
                Enabled = false;

                Form3 syncForm = new Form3(comboBox1.GetItemText(comboBox1.SelectedItem));
                syncForm.Show();
                //syncForm.SynchronizacjaWork(comboBox1.GetItemText(comboBox1.SelectedItem));
                syncForm.ZmianaLaba("Synchronizacja w toku");
                syncForm.SynchronizacjaStart();
                syncForm.Close();
                
                //ssyncForm.SynchronizacjaWork(comboBox1.GetItemText(comboBox1.SelectedItem));

                Enabled = true;

                AktualizacjaZadan();
            }
            else
            {
                MessageBox.Show("Brak zadań do przeprowadzenia synchronizacji");
            }




        }
    }
}
