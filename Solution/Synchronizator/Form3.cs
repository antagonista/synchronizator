﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SQLiteWrapper;
using System.IO;
using Ionic.Zip;
using FTP;


namespace Synchronizator
{
    public partial class Form3 : Form
    {

        public string obecnyKatalog;
        public string zadanieToWork;
        public int maxProgress = 5;
        public int licznikProgress = 0;
        
        public Form3(string zadanie)
        {
            InitializeComponent();
            obecnyKatalog = Directory.GetCurrentDirectory();

            zadanieToWork = zadanie;

            ZmianaLaba("Synchroniazcja w toku");

            progressBar1.Maximum = maxProgress;

            //label1.Text = "Przygotowanie do synchronizacji";

            //SynchronizacjaWork(zadanie);

            //Close();
            
        }

        public void SynchronizacjaStart()
        {
            SynchronizacjaWork(zadanieToWork);

        }

        public void ZmianaLaba(string txt)
        {
            label1.Text = txt;
            licznikProgress++;

            progressBar1.Increment(licznikProgress);

            //progressBar1.Maximum = ;
            Refresh();
        }


        public void SynchronizacjaWork(string zadanie)
        {


            //MessageBox.Show("START");


            ZmianaLaba("Przygotowanie synchronizacji");
            //label1.Text = "Przygotowanie synchronizacji";

            // sprawdzenie, czy sa jakies katalogi do zadania podpiete

            SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

            DataTable table = dbSQLite.ExecuteQuery("SELECT `id`,`ftp` FROM `zadania` WHERE `nazwa` = '" + zadanie + "';");


            DataTable table2 = dbSQLite.ExecuteQuery("SELECT count(*) FROM `zadania_dir` WHERE `id_zadanie` = '" + (int)table.Rows[0][0] + "';");


            if ((int)table2.Rows[0][0] > 0)
            //sa katalogi w zadaniu
            {

                // nastepuje synchronizacja

                // utworzenie folderu tymczasowego
                string nazwaDirBackup = string.Format("bu_{0:smhdMy}_{1}", DateTime.Now, zadanie);
                string dirBackupFull = obecnyKatalog + @"\temp\" + nazwaDirBackup;

                if (!Directory.Exists(dirBackupFull))
                {
                    Directory.CreateDirectory(dirBackupFull);
                }


                // synchronizacja all dir z zadania do tymczasowego
                ZmianaLaba("Synchronizacja w toku");
                //label1.Text = "Synchronizacja w toku";
                Synchronizacja sync = new Synchronizacja();

                DataTable table3 = dbSQLite.ExecuteQuery("SELECT `dir` FROM `zadania_dir` WHERE `id_zadanie` = '" + table.Rows[0][0] + "';");

                foreach (DataRow row in table3.Rows)
                {
                    sync.SynchAllFolderJednaStrona((string)row[0], dirBackupFull);
                }

                // synchronizacja w druga strone - z tymczasowego, do normalnych

                foreach (DataRow row in table3.Rows)
                {
                    sync.SynchAllFolderJednaStrona(dirBackupFull, (string)row[0]);
                }

                // synchronizacja zakonczona :)

                // tworzenie ZIP'a z backupu oraz czyszczenie po sobie

                //label1.Text = "Archiwizacja";
                ZmianaLaba("Archiwizacja");
                // wrzucenie pliku config do backupu

                //MessageBox.Show(string.Format("{0}{1}", obecnyKatalog, @"\temp\config"));
                //MessageBox.Show(string.Format("{0}{1}", dirBackupFull, @"\config"));
                //string.Format("{0}{1}", obecnyKatalog, @"\temp\config");
                //string.Format("{0}{1}", dirBackupFull, @"\config");

                

                

                

                // wyciaganie hasla do zipa z bazy

                DataTable table4 = dbSQLite.ExecuteQuery("SELECT `ConfigValue` FROM `config` WHERE `ConfigName` = 'haslo';");

                // przemycanie configa do backup
                //Directory.CreateDirectory(obecnyKatalog + @"\temp\config");
                //File.Copy(string.Format("{0}{1}", obecnyKatalog, @"\temp\config"), string.Format("{0}{1}", obecnyKatalog, @"\temp\config\config"));

                var zip = new ZipFile();

                zip.Password = Convert.ToString(table4.Rows[0][0]);
                //MessageBox.Show(string.Format("{0}{1}", obecnyKatalog, @"\temp\config"));
                
                zip.AddFile(string.Format("{0}{1}", obecnyKatalog, @"\temp\config"),@".\");
                //zip.AddFile(@".\temp\config",@".\");


                Directory.CreateDirectory(dirBackupFull + @"\config_soft");

                //MessageBox.Show(string.Format(@"{0}\{1}\config_soft\config.zip", obecnyKatalog, nazwaDirBackup));
                //zip.AddDirectory(string.Format("{0}{1}", obecnyKatalog, @"\temp\config\config");
                
                zip.Save(string.Format(@"{0}\temp\{1}\config_soft\config.zip", obecnyKatalog, nazwaDirBackup));

                


                // tworzenie zip'a
                zip = new ZipFile();

                zip.Password = Convert.ToString(table4.Rows[0][0]);
                zip.AddDirectory(dirBackupFull);

                zip.Save(obecnyKatalog + @"\" + nazwaDirBackup + ".zip");
                ZmianaLaba("Plik backup'u utworzono"); //4
                // EWENTUALNIE PAKOWANIE NA FTP

                if ((int)table.Rows[0][1] != 0)
                //jest ftp do zadania
                {

                    //MessageBox.Show("FTP SIE ZACZYNA");
                    progressBar1.Maximum = 6;
                    ZmianaLaba("Inicjalizacja polaczenia FTP");
                    
                    //label1.Text = "Inicjalizacja polaczenia FTP";
                    // pobieranie informacji na temat ftp
                    DataTable table5 = dbSQLite.ExecuteQuery("SELECT * FROM `ftp` WHERE `id_ftp` = '" + (int)table.Rows[0][1] + "';");

                    try
                    {


                        FtpClient ftp = new FtpClient((string)table5.Rows[0][2], (string)table5.Rows[0][3], (string)table5.Rows[0][4]);
                        ftp.Login();

                        //label1.Text = "Polaczono z FTP";
                        progressBar1.Maximum =7;
                        ZmianaLaba("Polaczono z FTP");
                        
                        if ((string)table5.Rows[0][5] != "")
                        //podano katalog dodatkowy
                        {
                            progressBar1.Maximum = 8;
                            ZmianaLaba("FTP: zmiana katalogu");
                            //label1.Text = "FTP: zmiana katalogu";
                            ftp.ChangeDir((string)table5.Rows[0][5]);
                        }
                        ZmianaLaba("FTP: upload");
                        //label1.Text = "FTP: upload";
                        //upload danych
                        ftp.Upload(obecnyKatalog + @"\" + nazwaDirBackup + ".zip");
                        progressBar1.Maximum = 9;
                        ZmianaLaba("FTP: zamykanie polaczenia");
                        //label1.Text = "FTP: zamykanie polaczenia";
                        ftp.Close();


                    }
                    catch
                    {
                        progressBar1.Maximum = 6;
                        ZmianaLaba("FTP: blad transferu");
                        
                        MessageBox.Show("Niestety transfer plikow na FTP sie nie udal!");
                    }


                }

                //czyszczenie po sobie

                Directory.Delete(dirBackupFull,true);

               

                MessageBox.Show("Synchronizacja - done!");

            }
            else
            // brak zadan
            {
                MessageBox.Show("Brak przypisanych katalogow do synchnorizacji do zadania!");
            }




            dbSQLite.CloseDatabase();


            //Close();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
