﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using System.IO.File;
using System.IO;
using SQLiteWrapper;
using Ionic.Zip;


namespace Synchronizator
{
    public partial class Form1 : Form 
    {

        public bool coSieDzieje;
        public string obecnyKatalog;
        
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //sprawdzanie czy istnieje plik konfiguracyjny
            obecnyKatalog = Directory.GetCurrentDirectory();

            if (!Directory.Exists(obecnyKatalog + @"\temp"))
            {
                // utworzenie
                Directory.CreateDirectory(obecnyKatalog + @"\temp");
            }
            

            if (File.Exists(obecnyKatalog + @"\config.zip"))
                //plik konfiguracyjny istnieje 
            {
                label1.Text = "Plik konfiguracyjny istnieje";
                label2.Text = "Podaj hasło:";
                button1.Text = "Wczytaj";
                coSieDzieje = true;
            }
            else
                //plik konfiguracyjny nie istnieje - utworzenie go
            {
                label1.Text = "Plik konfiguracyjny nie istnieje";
                label2.Text = "Podaj hasło zabezpieczające:";
                button1.Text = "Utwórz";
                coSieDzieje = false;
            }



        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (coSieDzieje == true)
            //plik konfiguracyjny istnieje
            {
                var zip = ZipFile.Read(obecnyKatalog+@"\config.zip");
                zip.Password = textBox1.Text;

                
                 try
                    {
                        
                        //probuje otworzyc zipa - sprawdzanie czy haslo jest dobre
                        zip.ExtractAll(obecnyKatalog + @"\temp");

                        
                        
                    }
                catch
                    {
                    MessageBox.Show("Haslo jest niepoprawne!");    
                    //!!! TUTAJ BEDZIE ZAPETLENIE JAK NIE BEDZIE DOBRE HASLO!
                        label2.Text = "NIEPOPRAWNE HASŁO!";
                        textBox1.Text = "";
                        Close();
                    }

                 //zip.RemoveSelectedEntries();
                
                zip = null;

                
                 //MessageBox.Show(string.Format(@"{0}\config.zip", obecnyKatalog)); 
                /*
                 try
                 {
                     File f = new File(string.Format(@"{0}\config.zip", obecnyKatalog));
                     //f.delete();
                     //File.Delete();
                 }
                 catch
                 {
                     MessageBox.Show("Nie udalo sie usunac"); 
                 }
                
                */
                
                    
            }
            else
            // plik konfiguracyjny nie istnieje
            {
                SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog+@"\temp\config");

                // inicjowanie podstawowej struktury bazy danych
                dbSQLite.ExecuteNonQuery("CREATE TABLE [config] ([ConfigName] TEXT, [ConfigValue] TEXT);");
                dbSQLite.ExecuteNonQuery("CREATE TABLE [zadania] ([id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,[nazwa] TEXT  NULL,[ftp] INTEGER NULL);");
                dbSQLite.ExecuteNonQuery("CREATE TABLE [zadania_dir] ([id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,[id_zadanie] INTEGER  NULL,[dir] TEXT  NULL);");
                dbSQLite.ExecuteNonQuery("CREATE TABLE [ftp] ([id_ftp] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,[nazwa] TEXT  NULL,[host] TEXT  NULL,[user] TEXT  NULL,[haslo] TEXT  NULL,[dir] TEXT  NULL);");
                
                
                dbSQLite.ExecuteNonQuery("INSERT INTO `config` VALUES('haslo','" + textBox1.Text + "');");


                dbSQLite.CloseDatabase();
                

                
            }

            // zamykanie tego okna - przechodzimy do prawdziwego prgoramu
            Enabled = false;
            Visible = false;
            //ActiveForm = false;

            //ActiveControl = false;


            

            Form2 form2 = new Form2();
            form2.ShowDialog();

            //usuwanie po sobie sladow!



            Application.Exit();



        }
    }
}
