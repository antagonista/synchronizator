﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Synchronizator
{
    class Synchronizacja
    {

        public void SynchAll(string[] zrodlowy, string docelowy)
        {

            foreach (string dirZrodlowy in zrodlowy)
            {
                SynchAllFolderJednaStrona(dirZrodlowy, docelowy);
            }


        }



        public void SynchAllFolderJednaStrona(string zrodlowy, string docelowy)
        {


            // synchronizacja glownego katalogu
            SynchrJednaStrona(zrodlowy, docelowy);

            // synchronizacja pod folderow
            string[] dirAllZrodlowy = Directory.GetDirectories(zrodlowy);

            foreach (string dirZrodlowy in dirAllZrodlowy)
            {
                //Console.WriteLine(dirZrodlowy);

                string zamiana;
                zamiana = dirZrodlowy;
                zamiana = zamiana.Replace(zrodlowy + @"\", docelowy+@"\");

                //Console.WriteLine(zamiana);

                if (Directory.Exists(zamiana)==false)
                    //katalog nie istnieje w docelowym - utworzenie
                {
                    //Console.WriteLine("tworzenie katalogu");
                    Directory.CreateDirectory(zamiana);
                }

                SynchrJednaStrona(dirZrodlowy, zamiana);
            }
            


        }
        
        
        public void SynchrJednaStrona(string @zrodlowyDIR, string @docelowyDIR)
        {

            

            
            //tworzenie tablic ze wszystkimi plikami
            

            // wszystkie pliki
            string[] plikiAllZrodlowy = Directory.GetFiles(zrodlowyDIR);
            


            //Console.WriteLine("----------");

            string zamianaTemp;
            string zamianaTemp2;
            
            // synchronizacja glownego katalogu


            foreach (string value in plikiAllZrodlowy)
            {

                // wyizylowanie samej sciezki pliku
                zamianaTemp = value;
                zamianaTemp = zamianaTemp.Replace(zrodlowyDIR + @"\", "");

                //podpiecie do docelowej sciezki

                zamianaTemp2 = System.IO.Path.Combine(docelowyDIR, zamianaTemp);

                // sprawdzamy czy taki dir istnieje w docelowym

                if (File.Exists(zamianaTemp2))
                // plik istnieje - dalsze porownywanie
                {
                    
                    //data ostatniej modyfikacji
                    DateTime data1 = File.GetLastWriteTime(zamianaTemp2); //\test 2 - do niego kopiujemy
                    DateTime data2 = File.GetLastWriteTime(value); //\test
                    TimeSpan dataRoznica = data2 - data1;
                    
                    
                    if (dataRoznica.Milliseconds > 0)
                    {
                        // zamieniamy
                        
                        File.Delete(zamianaTemp2);
                        File.Copy(value, zamianaTemp2);
                    }
                    
                }
                else
                // plik nie istnieje - kopiujemy
                {
                    File.Copy(value, zamianaTemp2);
                    //Console.WriteLine("kopiujemy!");
                }


            }

            // synchronizacja pod folderow



            


        }



    }
}
