﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SQLiteWrapper;
using System.IO;
using FTP;


namespace Synchronizator
{
    public partial class KonfiguracjaFTP : Form
    {

        public int idSesja = 0;
        public string obecnyKatalog;
        
        public KonfiguracjaFTP(string sesja="")
        {
            InitializeComponent();

            obecnyKatalog = Directory.GetCurrentDirectory();

            if (sesja != "")
            //edycja istniejacego ftp
            {

                SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

                DataTable table = dbSQLite.ExecuteQuery("SELECT count(*) FROM `ftp` WHERE `nazwa` = '" +sesja+ "';");

                if ((int)table.Rows[0][0] == 1)
                {

                    DataTable table2 = dbSQLite.ExecuteQuery("SELECT * FROM `ftp` WHERE `nazwa` = '" + sesja + "';");

                    // nadanie wartosci 
                    idSesja = (int)table2.Rows[0][0];
                    textBox1.Text = (string)table2.Rows[0][1];
                    textBox2.Text = (string)table2.Rows[0][2];
                    textBox3.Text = (string)table2.Rows[0][3];
                    textBox4.Text = (string)table2.Rows[0][4];
                    textBox5.Text = (string)table2.Rows[0][5];


                }
                else
                    //nieznany blad
                {
                    MessageBox.Show("Dziwne... cos nie fika - wystapil dziwny i nie zdiagnozowalny blad (: 'Don't worry be happy' ");
                    Close();
                }


                dbSQLite.CloseDatabase();


            }
            else
            //dodanie nowego ftp
            {

                //nic sie nie dzieje :)

            }


        }

        private void label3_Click(object sender, EventArgs e)
        {

        }


        public void AkutualizujPola()
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (idSesja != 0)
            // zapis edycji
            {

                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "")
                // nazwa nie moze byc pusta itd
                {
                    MessageBox.Show("Przed zapisem takie pola jak: nazwa, host, użytkownik czy hasło nie mogą być puste!");
                }
                else
                // wszystko dobrze - do przodu
                {
                    SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

                    // sprawdzenie czy wpisana nazwa jest unikalna!

                    DataTable table = dbSQLite.ExecuteQuery("SELECT count(*) FROM `ftp` WHERE `nazwa` = '" + textBox1.Text + "';");
                    DataTable table2 = dbSQLite.ExecuteQuery("SELECT `nazwa` FROM `ftp` WHERE `id_ftp` = '" + idSesja + "';");

                    //MessageBox.Show((string)table2.Rows[0][0]);

                    if ((int)table.Rows[0][0] == 0 || Convert.ToString(table2.Rows[0][0]) == textBox1.Text)
                        //nazwa jest unikalna lub zmiany nazwy nie bylo
                    {
                        dbSQLite.ExecuteNonQuery("UPDATE `ftp` SET `nazwa` = '" + textBox1.Text + "', `host` = '" + textBox2.Text + "', `user` = '" + textBox3.Text + "', `haslo` = '" + textBox4.Text + "', `dir` = '" + textBox5.Text + "' WHERE `id_ftp` = '" + idSesja + "';");
                        MessageBox.Show("Edycja danych przebiegla wzorowo!");
                        AkutualizujPola();
                    }
                    else
                        // nazwa sie powtarza
                    {
                        MessageBox.Show("Podana nazwa została już użyta! wprowadz inną!");
                    }

                    dbSQLite.CloseDatabase();

                    
                }



            }
            else
            // zapis nowego
            {

                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "")
                // nazwa nie moze byc pusta itd
                {
                    MessageBox.Show("Przed zapisem takie pola jak: nazwa, host, użytkownik czy hasło nie mogą być puste!");
                }
                else
                // wszystko dobrze - do przodu
                {
                    SQLiteBase dbSQLite = new SQLiteBase(obecnyKatalog + @"\temp\config");

                    // sprawdzenie czy wpisana nazwa jest unikalna!

                    DataTable table = dbSQLite.ExecuteQuery("SELECT count(*) FROM `ftp` WHERE `nazwa` = '" + textBox1.Text + "';");

                    if ((int)table.Rows[0][0] == 0)
                    //nazwa jest unikalna
                    {
                        dbSQLite.ExecuteNonQuery("INSERT INTO `ftp` (`nazwa`,`host`,`user`,`haslo`,`dir`) VALUES ('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + textBox5.Text + "');");

                        //przypisanie wartosci id do zmiennej

                        DataTable table2 = dbSQLite.ExecuteQuery("SELECT count(*) FROM `ftp` WHERE `nazwa` = '" + textBox1.Text + "';");

                        if ((int)table2.Rows[0][0] == 1)
                        // wszystko dobrze
                        {
                            DataTable table3 = dbSQLite.ExecuteQuery("SELECT `id_ftp` FROM `ftp` WHERE `nazwa` = '" + textBox1.Text + "';");

                            idSesja = (int)table3.Rows[0][0];
                        }
                        else
                            // cos nie tak
                        {
                            MessageBox.Show("Dziwne... cos nie fika - wystapil dziwny i nie zdiagnozowalny blad (: 'Don't worry be happy' ");
                        }


                        MessageBox.Show("Dodano nowe polaczenie FTP!");
                        AkutualizujPola();
                    }
                    else
                    // nazwa sie powtarza
                    {
                        MessageBox.Show("Podana nazwa została już użyta! wprowadz inną!");
                    }

                    dbSQLite.CloseDatabase();


                }


            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            // zamykanie okna

            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // testujemy sobie polaczenie :)

            Enabled = false;

            try
            {
                FtpClient ftp = new FtpClient(textBox2.Text, textBox3.Text, textBox4.Text);
                ftp.Login();
                ftp.Close();
                MessageBox.Show("Polaczono poprawnie!");
            }
            catch
            {
                MessageBox.Show("Blad z polaczeniem!");
            }


            Enabled = true;



        }
    }
}
