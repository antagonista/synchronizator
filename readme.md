# Synchronizator

prosta aplikacja do synchronizacji oraz tworzenia backup'ów plików zabezpieczonych hasłem w archiwum (z możliwością przetransferowania backup'u na serwer FTP). Napisana w C#.

## Video 
[![Watch the video](readme_files/Synchronizator.png)](readme_files/Synchronizator.mp4)
